package fr.comore.paramen.rank.ranks;

import java.util.Arrays;

public enum RankUnit {
    PLAYER("Default", 0, 10, "§a", "§aDefault", "§a"),
    MEMBER("Member", 1, 10, "§7[§2M§7] §2", "§2Member", "§2"),
    CANDY("Candy", 2, 10, "§7[§dCandy§7] §d", "§dCandy", "§d"),
    COOKIE("Cookie", 3, 10, "§7[§eCookie§7] §e", "§eCookie", "§e"),
    CAKE("Cake", 4, 10, "§7[§bCake§7] §b", "§bCake", "§b"),
    YT("YouTuber", 5, 8, "§7[§5YouTuber§7] §5", "§5YouTuber", "§5"),
    FAMOUS("Famous", 6, 8, "§7[§9Famous§7] §9", "§9Famous", "§9"),
    PARTNER("Partner", 7, 8, "§7[§6Partner§7] §6", "§6Partner", "§6"),
    BUILDER("Builder", 8, 7, "§7[§2Builder§7] §2", "§2Builder", "§2"),
    TRIALMOD("TrialMod", 9, 6, "§7[§e§oTrial-Mod§7] §e", "§e§oTrial-Mod", "§e§o"),
    MOD("Moderator", 10, 5, "§7[§3Moderator§7] §3", "§3Moderator", "§3"),
    SRMOD("SeniorMod", 11, 4, "§7[§5§oSenior-Mod§7] §5", "§5§oSenior-Mod", "§5§o"),
    ADMIN("Admin", 12, 3, "§7[§cAdmin§7] §c", "§cAdmin", "§c"),
    SENIORADMIN("SeniorAdmin", 13, 2, "§7[§c§oSenior-Admin§7] §c", "§c§oSenior-Admin", "§c§o"),
    MANAGER("Manager", 14, 2, "§7[§8Manager§7] §c", "§8Manager", "§8"),
    DEVELOPER("Developer", 15, 2, "§7[§bDeveloper§7] §b", "§bDeveloper", "§b"),
    COOWNER("CoOwner", 16, 2, "§7[§4Co-Owner§7] §4", "§4Co-Owner", "§4"),
    OWNER("Owner", 17, 1, "§7[§4Owner§7] §4", "§4Owner", "§4");

    private String name;
    private int power;
    private int scoreboardPower;
    private String prefix;
    private String nameColor;
    private String scoreboardPrefix;

    RankUnit(String name, int power, int scoreboardPower, String prefix, String nameColor, String scoreboardPrefix) {
        this.name = name;
        this.power = power;
        this.scoreboardPower = scoreboardPower;
        this.prefix = prefix;
        this.nameColor = nameColor;
        this.scoreboardPrefix = scoreboardPrefix;
    }

    public static RankUnit getByName(String name){
        return Arrays.stream(values()).filter(r -> r.getName().equalsIgnoreCase(name)).findAny().orElse(RankUnit.PLAYER);
    }

    public static RankUnit getByPower(int power){
        return Arrays.stream(values()).filter(r -> r.getPower() == power).findAny().orElse(RankUnit.PLAYER);
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public String getPrefix() {
        return prefix;
    }

    public String getNameColor() {
        return nameColor;
    }

    public String getScoreboardPrefix() {
        return scoreboardPrefix;
    }

    public int getScoreboardPower() {
        return scoreboardPower;
    }
}
