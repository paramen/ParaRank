package fr.comore.paramen.rank.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerPingListener implements Listener {

    @EventHandler
    public void onServerPing(ServerListPingEvent event) {
        event.setMotd(("&e&lEU &7❘ &e&lParamen Server\n" +
                        " &f* &f&lFollow us &e&l@ParamenEU&f&l! &f&lEnjoy &e&l;&f&l)").replace("&", "§"));
        event.setMaxPlayers(1028);
    }

}
