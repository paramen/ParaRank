package fr.comore.paramen.rank.accounts;

import fr.comore.paramen.rank.ParaRank;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.UUID;

public class OffineAccount {
    private static final String TABLE = "accounts";
    private String uuid;

    public OffineAccount(UUID uuid) {
        this.uuid = uuid.toString();
    }


    public RankUnit getRank(){
        return (RankUnit) ParaRank.getInstance().getMySQL().query("SELECT * FROM " + TABLE + " WHERE uuid='" + uuid + "'", rs -> {
            try {
                if(rs.next()){
                    return RankUnit.getByName(rs.getString("grade"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return RankUnit.PLAYER;
        });
    }

    public boolean hasTempRank(){
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT * FROM " + TABLE + " WHERE uuid='" + uuid + "'", rs -> {
            try {
                if(rs.next()){
                    return rs.getLong("grade_end") != -1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        });
    }

    public boolean rankIsExceeded(){
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT * FROM " + TABLE + " WHERE uuid='" + uuid + "'", rs -> {
            try {
                if(rs.next()){
                    return rs.getLong("grade_end") != -1 && rs.getLong("grade_end") < System.currentTimeMillis();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        });
    }

    public void setRank(RankUnit rank){
        ParaRank.getInstance().getMySQL().update("UPDATE " + TABLE + " SET grade='" + rank.getName() + "', grade_end='-1' WHERE uuid='" + uuid + "'");
    }

    public void setRank(RankUnit rank, long endInSeconds){
        ParaRank.getInstance().getMySQL().update("UPDATE " + TABLE + " SET grade='" + rank.getName() + "', grade_end='" + (endInSeconds * 1000 + System.currentTimeMillis()) + "' WHERE uuid='" + uuid + "'");
    }
}