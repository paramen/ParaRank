package fr.comore.paramen.rank.accounts;

import fr.comore.paramen.rank.ParaRank;
import fr.comore.paramen.rank.ranks.RankUnit;
import org.bukkit.entity.Player;

import java.sql.SQLException;

public class Account {
    private static final String TABLE = "accounts";
    private Player player;
    private String uuid;

    public Account(Player player) {
        this.player = player;
        uuid = player.getUniqueId().toString();
    }

    public Player getPlayer() {
        return player;
    }

    public static Account getAccount(Player player){
        return ParaRank.getInstance().getAccounts().stream().filter(a -> a.getPlayer() == player).findFirst().get();
    }

    public void setup(){
        ParaRank.getInstance().getAccounts().add(this);
        ParaRank.getInstance().getMySQL().query("SELECT * FROM " + TABLE + " WHERE uuid='" + uuid + "'", rs -> {
            try {
                if(!rs.next()){
                    ParaRank.getInstance().getMySQL().update("INSERT INTO " + TABLE + " (uuid, grade, grade_end, coins) VALUES ('" + uuid + "', '" + RankUnit.PLAYER.getName() + "', '-1', '0')");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        if(rankIsExceeded()){
            setRank(RankUnit.PLAYER);
        }
    }

    public void delete(){
        ParaRank.getInstance().getAccounts().remove(this);
    }

    public RankUnit getRank(){
        return (RankUnit) ParaRank.getInstance().getMySQL().query("SELECT * FROM " + TABLE + " WHERE uuid='" + uuid + "'", rs -> {
            try {
                if(rs.next()){
                    return RankUnit.getByName(rs.getString("grade"));
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return RankUnit.PLAYER;
        });
    }

    public boolean hasTempRank(){
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT * FROM " + TABLE + " WHERE uuid='" + uuid + "'", rs -> {
            try {
                if(rs.next()){
                    return rs.getLong("grade_end") != -1;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        });
    }

    public boolean rankIsExceeded(){
        return (boolean) ParaRank.getInstance().getMySQL().query("SELECT * FROM " + TABLE + " WHERE uuid='" + uuid + "'", rs -> {
            try {
                if(rs.next()){
                    return rs.getLong("grade_end") != -1 && rs.getLong("grade_end") < System.currentTimeMillis();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return false;
        });
    }

    public void setRank(RankUnit rank){
        ParaRank.getInstance().getMySQL().update("UPDATE " + TABLE + " SET grade='" + rank.getName() + "', grade_end='-1' WHERE uuid='" + uuid + "'");
    }

    public void setRank(RankUnit rank, long endInSeconds){
        ParaRank.getInstance().getMySQL().update("UPDATE " + TABLE + " SET grade='" + rank.getName() + "', grade_end='" + (endInSeconds * 1000 + System.currentTimeMillis()) + "' WHERE uuid='" + uuid + "'");
    }
}