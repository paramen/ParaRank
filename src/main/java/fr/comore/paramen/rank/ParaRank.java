package fr.comore.paramen.rank;

import fr.comore.paramen.rank.accounts.Account;
import fr.comore.paramen.rank.listeners.JoinListener;
import fr.comore.paramen.rank.listeners.ServerPingListener;
import fr.comore.paramen.rank.mysql.MySQL;
import org.apache.commons.dbcp2.BasicDataSource;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class ParaRank extends JavaPlugin {

    private static ParaRank instance;

    private BasicDataSource connectionPool;
    private MySQL mysql;

    private static List<Account> accounts;

    @Override
    public void onEnable() {
        instance = this;
        accounts = new ArrayList<>();
        initConnection();
        registerComponents();
        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public static ParaRank getInstance() {
        return instance;
    }

    private void initConnection(){
        connectionPool = new BasicDataSource();
        connectionPool.setDriverClassName("com.mysql.jdbc.Driver");
        connectionPool.setUsername("root");
        connectionPool.setPassword("manumagie0!");
        connectionPool.setUrl("jdbc:mysql://localhost:3306/practice_?autoReconnect=true");
        connectionPool.setInitialSize(1);
        connectionPool.setMaxTotal(10);
        mysql = new MySQL(connectionPool);
        mysql.createTables();
    }

    private void registerComponents() {
        PluginManager pluginManager = Bukkit.getServer().getPluginManager();
        pluginManager.registerEvents(new JoinListener(), this);
        pluginManager.registerEvents(new ServerPingListener(), this);
    }

    public MySQL getMySQL() {
        return mysql;
    }

    public static List<Account> getAccounts() {
        return accounts;
    }
}
